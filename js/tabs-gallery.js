document.addEventListener('DOMContentLoaded',function(){
  document.querySelectorAll('.choices__item').forEach(function(choicesItem) {
    choicesItem.addEventListener('click', function(event) {
      const value = event.currentTarget.dataset.value

      document.querySelectorAll('.gallery__tab').forEach(function(galleryTab) {
        galleryTab.classList.remove('gallery__active')
      })

      document.querySelector(`[data-target="${value}"]`).classList.add('gallery__active')
      
    })
  })
})