window.addEventListener('DOMContentLoaded', function() {
  document.querySelector('#burger1024').addEventListener('click',function(){
    document.querySelector('#menu').classList.add('menu__clicked')
  })
  document.querySelector('#burger320').addEventListener('click',function(){
    document.querySelector('#menu').classList.add('menu__clicked')
  })
})

window.addEventListener('DOMContentLoaded', function() {
  document.querySelector('#close').addEventListener('click',function(){
    document.querySelector('#menu').classList.remove('menu__clicked')
  })
})

window.addEventListener('DOMContentLoaded', function() {
  document.querySelector('#loopa1024').addEventListener('click',function(){
    document.querySelector('#search1024').classList.toggle('search__clicked')
    document.querySelector('#close1024').classList.toggle('search__clicked')
    document.querySelector('#searchblock1024').classList.toggle('search-block-1024__clicked')
  })
  document.querySelector('#close1024').addEventListener('click', function() {
    document.querySelector('#searchblock1024').classList.toggle('search-block-1024__clicked')
    document.querySelector('#burger1024').classList.toggle('logo-disabled')
    document.querySelector('#logo1024').classList.toggle('logo-disabled')
    document.querySelector('#close1024').classList.toggle('search__clicked')
    document.querySelector('#search1024').classList.toggle('search__clicked')
  })
})

window.addEventListener('DOMContentLoaded', function() {
  const mediaQuery = window.matchMedia('(max-width: 1000px)')
if (mediaQuery.matches) {
  document.querySelector('#loopa1024').addEventListener('click',function(){
    document.querySelector('#burger1024').classList.toggle('logo-disabled')
    document.querySelector('#logo1024').classList.toggle('logo-disabled')
  })
}
})

window.addEventListener('DOMContentLoaded', function() {
  document.querySelector('#loopa320').addEventListener('click',function(){
    document.querySelector('#searchblock320').classList.add('search-block__320__clicked')
  })
  document.querySelector('#searchblock320close').addEventListener('click',function(){
    document.querySelector('#searchblock320').classList.remove('search-block__320__clicked')
  })
})
