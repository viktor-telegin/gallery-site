document.addEventListener('DOMContentLoaded',function(){
  document.querySelectorAll('.catalog__tabs-item').forEach(function(catalogTabsItem) {
    catalogTabsItem.addEventListener('click', function(event) {
      const path = event.currentTarget.dataset.path

      document.querySelectorAll('.catalog__content').forEach(function(galleryContent) {
        galleryContent.classList.remove('catalog__show')
      })
      document.querySelectorAll('.catalog__tabs-item').forEach(function(workListItemLink) {
        workListItemLink.classList.remove('catalog__tabs-item__show')
      })
      document.querySelector(`[data-target="${path}"]`).classList.add('catalog__show')
      document.querySelector(`[data-path="${path}"]`).classList.add('catalog__tabs-item__show')

      $('.accordion').accordion("refresh");
    })
  })
})

/*$( function() {
  $( "#accordion" ).accordion({
    collapsible: true,
    active: false,
    heightStyle: "content"
  });
} );*/

document.addEventListener('DOMContentLoaded', function () {
  $( ".accordion" ).accordion({
      collapsible: true,
      active: 0,
      icons: false,
      heightStyle: 'content'
    });
});
