const validation = new JustValidate('#form');

validation
  .addField('#name', [
    {
      rule: 'required',
      errorMessage: 'Заполните',
      color: '#D11616',
    },
    {
      rule: 'minLength',
      value: 3,
      errorMessage: 'Мало букв',
      
    },
    {
      rule: 'maxLength',
      value: 30,
      errorMessage: 'Много букв',
    },
  ])
  .addField('#number', [
    {
      rule: 'required',
      errorMessage: 'Заполните',
    },
    {
      rule: 'number',
      errorMessage: 'Недопустимый формат',
    },
  ]);

 