const swiperturbo = new Swiper('.swiper-turbo', {

  slideActiveClass: 'swiper-turbo-slide-active',
  // Optional parameters
  direction: 'horizontal',
  slidesPerView: 1,
  grid: {
    rows: 1,
    fill: "row"
  },
  spaceBetween: 20,

  breakpoints: {
    700: {
      slidesPerView: 2,
      /*grid: {
        rows: 1
      },*/
      spaceBetween: 34
    },

    1000: {
      slidesPerView: 2,
      /*grid: {
        rows: 1
      },*/
      spaceBetween: 50
    },

    1600: {
      slidesPerView: 3,
      /*grid: {
        rows: 1
      },*/
      spaceBetween: 50
    }
  },

  // If we need pagination
  pagination: {
    el: '.swiper-pagination__turbo',
    type: 'fraction'
  },

  // Navigation arrows
  navigation: {
    nextEl: '.swiper-button-next-turbo',
    prevEl: '.swiper-button-prev-turbo',
  },

  // And if we need scrollbar
  scrollbar: {
    el: '.swiper-scrollbar',
  },
  a11y: false,
});
