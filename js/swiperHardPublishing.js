    if (window.matchMedia("(min-width: 700px)").matches) {

      const swiperHard = new Swiper('.swiper-hard', {
        direction: 'horizontal',
        pagination: {
          el: '.swiper-pagination-hard',
          type: 'fraction'
        },
        navigation: {
          nextEl: '.swiper-button-next-hard',
          prevEl: '.swiper-button-prev-hard',
        },

        //slidesPerView: 7,
        //rows: 4,

        breakpoints: {
          700: {
            slidesPerView: 2,
            rows: 1,
            spaceBetween: 34
          },
          1000: {
            slidesPerView: 2,
            rows: 1,
            spaceBetween: 49
          },
          1600: {
            slidesPerView: 3,
            rows: 1,
            spaceBetween: 50
          }
        }
      });
    }
    else {
      $(window).resize(function() {
        //resize just happened, pixels changed
        swiperHard.destroy();
      });
    }




