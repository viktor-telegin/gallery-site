window.addEventListener('DOMContentLoaded', function() {
  document.querySelector('#realism').addEventListener('click',function(){
    document.querySelector('#realism').classList.toggle('clicked')
    document.querySelector('#content-impres, #content-postimpres, #content-avangard, #content-futurism').classList.remove('header__list-item__dropdown-shown'),
    document.querySelector('#content-realism').classList.toggle('header__list-item__dropdown-shown')
  })
  document.querySelector('#impres').addEventListener('click',function(){
    document.querySelector('#impres').classList.toggle('clicked')
    document.querySelector('#content-realism', '#content-postimpres', '#content-avangard', '#content-futurism').classList.remove('header__list-item__dropdown-shown'),
    document.querySelector('#content-impres').classList.toggle('header__list-item__dropdown-shown')
  })
  document.querySelector('#postimpres').addEventListener('click',function(){
    document.querySelector('#postimpres').classList.toggle('clicked')
    document.querySelector('#content-impres', '#content-realism', '#content-avangard', '#content-futurism').classList.remove('header__list-item__dropdown-shown'),
    document.querySelector('#content-postimpres').classList.toggle('header__list-item__dropdown-shown')
  })
  document.querySelector('#avangard').addEventListener('click',function(){
    document.querySelector('#avangard').classList.toggle('clicked')
    document.querySelector('#content-postimpres', '#content-impres', '#content-postimpres', '#content-realism', '#content-futurism').classList.remove('header__list-item__dropdown-shown'),
    document.querySelector('#content-avangard').classList.toggle('header__list-item__dropdown-shown')
  })
  document.querySelector('#futurism').addEventListener('click',function(){
    document.querySelector('#futurism').classList.toggle('clicked')
    document.querySelector('#content-avangard', '#content-impres', '#content-postimpres', '#content-avangard', '#content-realism').classList.remove('header__list-item__dropdown-shown'),
    document.querySelector('#content-futurism').classList.toggle('header__list-item__dropdown-shown')
  })
})

