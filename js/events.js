window.addEventListener('DOMContentLoaded', function() {
  document.querySelector('#events__btn').addEventListener('click',function(){
    document.querySelector('#events__block-one').classList.remove('events__block__hidden')
    document.querySelector('#events__block-two').classList.remove('events__block__hidden')
    document.querySelector('#events__block-three').classList.remove('events__block__hidden')

    document.querySelector('#events__btn').classList.add('events__block__hidden')
  })
})